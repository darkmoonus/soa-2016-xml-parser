package XMLParser;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by darkmoonus on 2/17/16.
 */
public class XMLParser {
    private static void printNote(NodeList nodeList, int position) {
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                for (int i = 0; i < position; i++) System.out.print("--");
                System.out.println(tempNode.getNodeName());
                if (tempNode.hasChildNodes()) {
                    printNote(tempNode.getChildNodes(), position + 1);
                }
            }
        }
    }

    public static void main(String args[]) {
        try {
            File file = new File("src/XMLParser/input.xml");
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            if (doc.hasChildNodes()) {
                printNote(doc.getChildNodes(), 0);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

